package com.good.app.login

import com.good.app.R
import com.good.app.base.BaseActivity
import com.good.app.base.BaseFragment
import com.good.app.barutil.StatusBarUtil
import com.good.app.barutil.SystemBarManager

class LoginActivity : BaseActivity() {
    override fun getFragment(): BaseFragment {
        return LoginFragment.getInstance()
    }
    override fun statusBar() {
        StatusBarUtil.setStatusBarMode(this, true, R.color.c_ffffff)
        SystemBarManager.set(this)
    }
}
