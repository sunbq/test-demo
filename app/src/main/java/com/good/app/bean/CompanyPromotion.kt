package com.good.app.bean

data class CompanyPromotion(
    /**
     * 等级说明
     */ var record: Int? = null,
            /**
             * 通道最低费率
             */
            var rate: String? = null,
                    /**
                     * 通道最低清算费率
                     */
                    var clearingFee: String? = null,
                            /**
                             * 等级
                             */
                            var level: Int? = null) {
}