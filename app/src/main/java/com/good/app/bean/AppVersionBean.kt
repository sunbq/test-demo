package com.good.app.bean

data class AppVersionBean(
    var id: String? = null,

    /**
     * D 包名
     */
    var packageName: String? = null,

    /**
     * D 版本名称
     */
    var versionName: String? = null,

    /**
     * D 版本号
     */
    var versionCode: Int? = null,

    /**
     * D 系统类型
     * 1 - Android
     * 2 - IOS
     */
    var type: Int? = null,

    /**
     * D 是否强制更新
     * 0 - 不强制更新
     * 1 - 强制更新
     */
    var forceUpdate: Boolean? = null,

    /**
     * D app的下载路径
     */
    var updateUrl: String? = null,

    /**
     * D app的更新说明
     */
    var remark: String? = null,

    /**
     * D 更新文件大小
     */
    var filesize: String? = null,

    /**
     * D 更新信息创建日期
     */
    var createdAt: Long? =
        null,

    /**
     * D 更新信息是否有效
     */
    var isTrash: Boolean? =
        null
)