package com.good.app.bean

data class RealNameCertification(var id: Int? = null,
            var realName: String? = null,
                    var credentialsNumber: String? = null,
                            var jmAuthStatus: Int? = null) {
}