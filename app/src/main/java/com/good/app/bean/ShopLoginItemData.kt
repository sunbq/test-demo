package com.good.app.bean

data class ShopLoginItemData( var value: String? = null,
            var key: String? = null,
                    var hidden: String? = null,
                            var index: Int? = null,
                                    var label: String? = null,
                                            var href: String? = null) {
}