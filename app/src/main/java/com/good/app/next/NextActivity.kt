package com.good.app.test

import com.good.app.R
import com.good.app.base.BaseActivity
import com.good.app.base.BaseFragment
import com.good.app.barutil.StatusBarUtil
import com.good.app.barutil.SystemBarManager
import com.good.app.util.IntentMsg

class NextActivity : BaseActivity() {
    override fun getFragment(): BaseFragment {

        return NextFragment.getInstance(intent.getStringExtra(IntentMsg.MSG),intent.getIntExtra(IntentMsg.TIME,0),intent.getIntExtra(IntentMsg.COUNT,0))
    }
    override fun statusBar() {
        StatusBarUtil.setStatusBarMode(this, true, R.color.c_ffffff)
        SystemBarManager.set(this)
    }
}
