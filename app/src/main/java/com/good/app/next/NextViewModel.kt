package com.good.app.test

import androidx.lifecycle.MutableLiveData
import com.good.app.base.BaseViewModel
import com.good.app.bean.AppVersionBean
import com.good.app.net.NetRepository

class NextViewModel: BaseViewModel() {

    var isShowLoginButton = MutableLiveData<Boolean>()
    var tvResult = MutableLiveData<String>()

    var submitEvent = MutableLiveData<String>()

    init {
        isShowLoginButton.value = false
    }

}