package com.good.app.util

import android.app.Activity
import android.content.Context
import android.content.Intent

object AcUtils {

    fun launchActivity(context: Context?, activity: Class<out Activity>, msg: String?,time:Int,count:Int) {
        if (context == null) {
            return
        }

        val intent = Intent(context, activity)

        if (msg != null) {
            intent.putExtra(IntentMsg.MSG, msg)
        }

        if (time!=null){
            intent.putExtra(IntentMsg.TIME,time)

        }

        if (count!=null){
            intent.putExtra(IntentMsg.COUNT,count)

        }
        context.startActivity(intent)
    }

}