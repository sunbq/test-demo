package com.good.app.util

import java.io.Serializable

class IntentMsg : Serializable {

    var amount: Float = 0.toFloat()

    var type = -1

    lateinit var Id: String

    var Error: String? = null

    var Title: String? = null

    var url: String? = null

    var selected: Boolean = false

    var Content: String? = null

    var isLocal: Boolean = false
    var isShowPayWay: Boolean = false

    var receiptStyle: Int = 0

    var name: String? = null
    var phone: String? = null

    var timeStart: String? = null
    var timeEnd: String? = null
    var sumMoney: String? = null
    var balance: String? = null
    var count: String? = null
    var areaCode: String? = null

    var province: String? = null
    var city: String? = null

    var cardNo: String? = null
    var Type: String? = null

    var planId: String? = null
    var cardId: String? = null
    var businessType: String? = null
    var tradeMode: Int = 0
    var ChannelName: String? = null
    var selectVipPosition: Int = 0
    var VipOrPos: String? = null
    var orderCode: String? = null
    var shopNo: String? = null
    var teamCount: String? = null
    var queryid: String? = null
    var querydate: String? = null

    constructor() {}

    constructor(id: String) {
        this.Id = id
    }

    companion object {

        val MSG = "IntentMsg"
        val TIME:String = "Time"
        val COUNT:String = "Count"
        val PLATFORM_BANK_CARD = "IntentMsg"
    }


}