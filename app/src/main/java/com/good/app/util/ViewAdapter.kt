package com.good.app.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object ViewAdapter {

    @BindingAdapter("image")
    @JvmStatic
    fun setImage(view: ImageView, img: Int?) {
        Glide.with(view.context).load(img).into(view)
    }

}