package com.good.app.base

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {

    val TAG = BaseViewModel::class.java.simpleName

    val PAGE_SIZE = 15

    /**
     * 错误提示信息事件
     */
    val showMessageEvent = ObservableField<String>()

    fun showMessage(message: String) {
        showMessageEvent.set(message)
    }

}