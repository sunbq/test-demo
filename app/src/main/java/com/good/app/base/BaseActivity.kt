package com.good.app.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.good.app.R
import com.good.app.util.ActivityUtils

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statusBar()
        setContentView(R.layout.activity_base)
        setupViewFragment(getFragment())
    }

    abstract fun statusBar()

    abstract fun getFragment(): BaseFragment

    private fun setupViewFragment(baseFragment: BaseFragment) {
        var fragment = supportFragmentManager.findFragmentById(R.id.contentFrame) as BaseFragment?
        if (fragment == null) {
            fragment = baseFragment
            ActivityUtils.replaceFragmentInActivity(
                supportFragmentManager,
                fragment,
                R.id.contentFrame
            )
        }
    }

}
