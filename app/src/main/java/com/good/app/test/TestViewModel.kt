package com.good.app.test

import androidx.lifecycle.MutableLiveData
import com.good.app.base.BaseViewModel

class TestViewModel: BaseViewModel() {

    var isShowLoginButton = MutableLiveData<Boolean>()
    var tvCounts = MutableLiveData<String>()
    var tvTimes = MutableLiveData<String>()

    var submitEvent = MutableLiveData<String>()

    init {
        isShowLoginButton.value = false
    }

     fun timeModel(time:Int):String{
        var mSec:String=""
        var mMin:String=""
        // 秒数
        val sec = time % 60
        // 分钟数
        // 分钟数
        val min = (time - sec) / 60
        // 如果秒数没超过10就在他前面加个0
        // 如果秒数没超过10就在他前面加个0
        if (sec < 10) {
            mSec = "0$sec"
        } else {
            mSec = sec.toString()
        }
        // 如果分钟数没超过10就在他前面加个0
        // 如果分钟数没超过10就在他前面加个0
        if (min < 10) {
            mMin = "0$min"
        } else {
            mMin = min.toString()
        }
        // 打印最终的结果
        // 打印最终的结果
        return  mMin.toString() + ":" + mSec;
    }
    fun submit(){
        submitEvent.postValue("submit")
    }
}