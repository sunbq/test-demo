package com.good.app.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.good.app.base.BaseFragment
import com.good.app.databinding.ActivityTestBinding
import com.good.app.util.AcUtils

class TestFragment: BaseFragment() {

    lateinit var mBinding: ActivityTestBinding
    lateinit var mViewModel: TestViewModel
     var result: Double =0.0;
    companion object{
        fun getInstance(): TestFragment {
            return TestFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = ActivityTestBinding.inflate(inflater,container,false)
        mViewModel = ViewModelProvider(this).get(TestViewModel()::class.java)
        mBinding?.let {
            it.viewModel = mViewModel
            it.setLifecycleOwner(this)
        }
        setupListener()
        return mBinding.root
    }
    fun setupListener(){
        mViewModel.submitEvent.observe(viewLifecycleOwner, Observer {
            if (mViewModel.tvCounts==null||mViewModel.tvCounts.equals("")){
                Toast.makeText(activity,"请输入数字",Toast.LENGTH_SHORT).show()
                return@Observer
            }

            if (mViewModel.tvTimes==null||mViewModel.tvTimes.equals("")){
                Toast.makeText(activity,"请输入时间",Toast.LENGTH_SHORT).show()
                return@Observer
            }
            result= mViewModel.tvCounts.value!!.toDouble()!! * mViewModel.tvTimes.value!!.toDouble()

            AcUtils.launchActivity(activity,NextActivity::class.java,result.toString(),
                mViewModel.tvTimes.value!!.toInt(), mViewModel.tvCounts.value!!.toInt())
        })

    }


}