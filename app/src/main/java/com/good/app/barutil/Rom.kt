package com.good.app.barutil

import android.os.Build
import android.text.TextUtils
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class Rom {

    //伴生对象
    companion object isRom{
        //放入伴生对象实现java中的static属性
        val ROM_MIUI = "MIUI"
        val ROM_EMUI = "EMUI"
        val ROM_FLYME = "FLYME"
        val ROM_OPPO = "OPPO"
        val ROM_SMARTISAN = "SMARTISAN"
        val ROM_VIVO = "VIVO"
        val ROM_QIKU = "QIKU"

        val KEY_VERSION_MIUI = "ro.miui.ui.version.name"
        val KEY_VERSION_EMUI = "ro.build.version.emui"
        val KEY_VERSION_OPPO = "ro.build.version.opporom"
        val KEY_VERSION_SMARTISAN = "ro.smartisan.version"
        val KEY_VERSION_VIVO = "ro.vivo.os.version"

        var sName: String? = null
        var sVersion: String? = null

        fun isEmui(): Boolean {
            return check(ROM_EMUI)
        }

        fun isMiui(): Boolean {
            return check(ROM_MIUI)
        }

        fun isVivo(): Boolean {
            return check(ROM_VIVO)
        }

        fun isOppo(): Boolean {
            return check(ROM_OPPO)
        }

        fun isFlyme(): Boolean {
            return check(ROM_FLYME)
        }

        fun is360(): Boolean {
            return check(ROM_QIKU) || check(
                "360"
            )
        }

        fun isSmartisan(): Boolean {
            return check(ROM_SMARTISAN)
        }

        fun getName(): String? {
            if (sName == null) {
                check("")
            }
            return sName
        }

        fun getVersion(): String? {
            if (sVersion == null) {
                check("")
            }
            return sVersion
        }

        fun check(rom: String): Boolean {
            if (sName != null) {
                return sName == rom
            }
            when(sVersion){
                getProp(KEY_VERSION_MIUI) -> sName =
                    ROM_MIUI
                getProp(KEY_VERSION_EMUI) -> sName =
                    ROM_EMUI
                getProp(KEY_VERSION_OPPO) -> sName =
                    ROM_OPPO
                getProp(KEY_VERSION_VIVO) -> sName =
                    ROM_VIVO
                getProp(KEY_VERSION_SMARTISAN) -> sName =
                    ROM_SMARTISAN
                else ->{
                    sVersion = Build.DISPLAY
                    if (sVersion!!.toUpperCase().contains(ROM_FLYME)) {
                        sName =
                            ROM_FLYME
                    } else {
                        sVersion = Build.UNKNOWN
                        sName = Build.MANUFACTURER.toUpperCase()
                    }
                }
            }
            return sName == rom
        }

        fun isMIUI9(): Boolean {
            val prop =
                getProp(KEY_VERSION_MIUI)
            return isMiui() && !TextUtils.isEmpty(prop) && (prop!!.toUpperCase() == "V9" || prop.toUpperCase() == "V10")
        }

        fun getProp(name: String): String? {
            var line: String? = null
            var input: BufferedReader? = null
            try {
                val p = Runtime.getRuntime().exec("getprop $name")
                input = BufferedReader(InputStreamReader(p.inputStream), 1024)
                line = input.readLine()
                input.close()
            } catch (ex: IOException) {
                return null
            } finally {
                if (input != null) {
                    try {
                        input.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }
            return line
        }
    }
}