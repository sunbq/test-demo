package com.good.app.net

import android.app.Application
import android.text.TextUtils
import com.good.app.bean.*
import com.good.app.util.SpTagConst
import com.good.app.util.SpUtils

class SessionManager private constructor() {
    /**
     * 用户登录状态
     */
    private var token: String? = null

    /**
     * 用户信息
     */
    private var user: User? = null

    /**
     * 登录信息
     */
    var loginUser: User? = null
        set(user) {
            if (TextUtils.isEmpty(user!!.nickname.toString())) {
                user.copy(nickname = "未设置昵称")
            }
            field = user
        }

    /**
     * 商城登录信息
     */
    private var lgd: ShopLoginData? = null

    private var levelInfos: List<CompanyLevel>? = null

    private var companyPromotions: List<CompanyPromotion>? = null

    var realNameCertification: RealNameCertification? = null
        private set

    private var application: Application? = null

    fun InitSessionManager() {
        if (sessionManager != null) {
            this.user = null
            this.token = null
            this.lgd = null
        }
    }

    fun init(application: Application) {
        this.application = application
    }

    fun setToken(token: String) {
        if (!TextUtils.isEmpty(token)) {
            this.token = token
            SpUtils.put(application!!.applicationContext, SpTagConst.TOKEN, token)
        }
    }

    fun getToken(): String? {
        if (TextUtils.isEmpty(token)) {
            this.token = SpUtils.get(application!!.applicationContext, SpTagConst.TOKEN, "") as String?
        }
        return this.token
    }

    fun getUser(): User? {
        return this.user
    }

    fun setUser(user: User) {
        if (TextUtils.isEmpty(user.nickname)) {
            user.copy(nickname = "未设置昵称")
        }
        this.user = user
    }

    fun getLgd(): ShopLoginData? {
        return lgd
    }

    fun setLgd(lgd: ShopLoginData) {
        if (TextUtils.isEmpty(lgd.nickName)) {
            lgd.copy(nickName = "未设置昵称")
        }
        this.lgd = lgd
    }

    fun setCertification(certification: RealNameCertification) {
        this.realNameCertification = certification
    }

    fun setLevelInfos(levelInfos: List<CompanyLevel>) {
        this.levelInfos = levelInfos
    }

    fun getLevelName(level: Int): String? {
        if (levelInfos == null) {
            return "访客"
        }
        for (info in levelInfos!!) {
            if (info.level === level) {
                return info.name
            }
        }
        return "访客"
    }

    fun setCompanyPromotion(companyPromotions: List<CompanyPromotion>) {
        this.companyPromotions = companyPromotions
    }

    fun getCompanyPromotion(level: Int): CompanyPromotion? {
        if (companyPromotions == null) {
            return null
        }
        for (companyPromotion in this.companyPromotions!!) {
            if (companyPromotion.level === level) {
                return companyPromotion
            }
        }
        return null
    }

    companion object {
        private var sessionManager: SessionManager? = null
        val instance: SessionManager?
            get() {
                if (sessionManager == null) {
                    synchronized(SessionManager::class.java) {
                        sessionManager = SessionManager()
                    }
                }
                return sessionManager
            }
    }
}