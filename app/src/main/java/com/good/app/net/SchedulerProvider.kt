package com.good.app.net

import androidx.annotation.NonNull
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerProvider// Prevent direct instantiation.
private constructor() {

    @NonNull
    fun computation(): Scheduler {
        return Schedulers.computation()
    }

    @NonNull
    fun io(): Scheduler {
        return Schedulers.io()
    }

    @NonNull
    fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    /*class RxStreamHelper {
        fun <T> io_Main(): ObservableTransformer<BaseResult<T>, T> {
            return ObservableTransformer { upstream ->
                upstream.subscribeOn(Schedulers.io())
                    //解析data层，剔除 code /msg
                    .flatMap { tBaseModel ->
                        if (tBaseModel.success) {
                            Observable.just(tBaseModel.data!!)
                        } else Observable.error(ApiException(tBaseModel.errorCode.toInt(), tBaseModel.errorMsg))
                    }
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }
    }*/

    fun <T> applySchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { observable ->
            observable.subscribeOn(io())
                .observeOn(ui())
        }
        /*{ Observable ->
            Observable.subscribeOn(io())
                .observeOn(ui())
        }*/
       /* return { observable ->
            observable.subscribeOn(io())
                .observeOn(ui())
        }*/
    }



    companion object{
        fun getInstance(): SchedulerProvider {
            return SchedulerProvider()
        }
    }
    /*companion object {

        private var INSTANCE: SchedulerProvider? = null

        val instance: SchedulerProvider
            @Synchronized get() {
                if (INSTANCE == null) {
                    INSTANCE = SchedulerProvider()
                }
                return INSTANCE
            }
    }*/
}