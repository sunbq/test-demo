package com.good.app.net

import com.good.app.bean.AppVersionBean
import io.reactivex.Observable

class NetRepository {

    /**
     * 检查更新
     */
    fun check(packageName: String, versionCode: String): Observable<AppVersionBean> {
        return NetworkManager.instance.create(NetService::class.java)
            .check("1", packageName, versionCode)
            .compose(ResponseTransfer.handleResult())
            .compose(SchedulerProvider.getInstance().applySchedulers())
    }


}