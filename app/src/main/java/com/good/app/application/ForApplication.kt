package com.good.app.application

import androidx.multidex.MultiDexApplication
import com.good.app.net.NetworkManager
import com.good.app.net.SessionManager

class ForApplication : MultiDexApplication() {

    //供外部获取全局的上下文
    companion object{
        private var mInstance: ForApplication? = null
        @Synchronized
        fun getInstance(): ForApplication? {
            return mInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        NetworkManager.instance.init()
        SessionManager.instance!!.init(this)
    }

}